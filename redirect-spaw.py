import socket
import random
url_list = ['https://www.aulavirtual.urjc.es/', 'https://gitlab.etsit.urjc.es/', 'https://myapps.urjc.es/myapps',
            'https://github.com/', 'https://www.youtube.com/watch?v=3GLbcghUbGM']

# Parameters of the Server.
Host = 'localhost'
Port = 2412

Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
Socket.bind(('localhost', Port))


Socket.listen(5)
random.seed()

# Function that creates an Answer of the Server to the Client.
def answer_url():
    url_random = random.choice(list_url)
    answer = "HTTP/1.1 302 Temporary Redirect  \r\n" \
             + "Location:" + answer_url().encode('ascii') + url_random + "\r\n\r\n"
    return answer


try:
    print("Serving at port", Port)
    while True:
        print("Waiting for connections")
        (recvSocket, address) = Socket.accept()
        print("Connected by", address)
        print(recvSocket.recv(2048))

except KeyboardInterrupt:
    print("Socket closed")
    Socket.close()
